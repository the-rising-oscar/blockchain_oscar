import hashlib
from datetime import datetime


class Block(object):
    def __init__(self, index, timestamp, transactions, previousHash):
        self.index = index
        self.timestamp = timestamp
        self.transactions = transactions
        self.previousHash = previousHash
        self.nonce = 0

    def calculateHash(self):
        strToHash = str(self.index) + str(self.timestamp) + str(self.transactions) + str(self.previousHash) + str(self.nonce)
        return hashlib.sha256(strToHash.encode(encoding='utf-8', errors='strict')).hexdigest()


class BlockChain(object):
    difficulty = 5
    maxNonce = 2**32
    target = 2 ** (256-difficulty)

    def __init__(self):
        self.chain = []
        self.create_genesis_block()

    def create_genesis_block(self):
        genesis_block = Block(index=0, timestamp=datetime.utcnow(), transactions=[], previousHash="0")
        genesis_block.hash = genesis_block.calculateHash()
        self.chain.append(genesis_block)

    @property
    def last_block(self):
        return self.chain[-1]

    def mine(self, transactions):
        last_block = self.last_block
        new_block = Block(index=last_block.index + 1,
                          transactions=transactions,
                          timestamp=datetime.utcnow(),
                          previousHash=last_block.hash)

        proof = self.proof_of_work(new_block)
        new_block.hash = proof
        self.chain.append(new_block)


    def proof_of_work(self, block):
        for n in range(self.maxNonce):
            Hash = block.calculateHash()
            if int(Hash, 16) <= self.target:
                return Hash
            else:
                block.nonce += 1


oscar_coin = BlockChain()

# oscar_coin.mine("str1")
# oscar_coin.mine("str1")
# oscar_coin.mine("str1")
# oscar_coin.mine("str1")
# oscar_coin.mine("str1")

# for i in oscar_coin.chain:
#     print('hash: ',i.hash)
#     print('previous hash: ', i.previousHash)
#     print('transactions: ', i.transactions)
#     print('nonce: ', i.nonce)
#     print("\n")
